import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { AuthService } from './auth.service';
import { UsersService } from './users.service';
import { User } from './user.entity';
import { NotFoundException } from '@nestjs/common';

describe('UsersController', () => {
  let controller: UsersController;
  let fakeUsersService: Partial<UsersService>;
  let fakeAuthService: Partial<AuthService>;
  const testEmail = 'test@test.com';

  beforeEach(async () => {
    fakeUsersService = {
      findOne: (id: number) => {
        return Promise.resolve({
          id,
          email: testEmail,
          password: 'test',
        } as User);
      },
      find: (email: string) => {
        return Promise.resolve([
          {
            id: 1,
            email,
            password: 'test',
          } as User,
        ]);
      },
      remove: (id: number) => {
        return Promise.resolve({
          id,
          email: testEmail,
          password: 'test',
        } as User);
      },
      update: (id: number, attrs: Partial<User>) => {
        return Promise.resolve({
          id,
          ...attrs,
        } as User);
      },
    };
    fakeAuthService = {
      signin: (email: string, password: string) => {
        return Promise.resolve({
          id: 1,
          email,
          password,
        } as User);
      },
    };
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: AuthService,
          useValue: fakeAuthService,
        },
        {
          provide: UsersService,
          useValue: fakeUsersService,
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('finds a list of users by email', async () => {
    const users = await controller.findAllUsers(testEmail);
    expect(users.length).toEqual(1);
    expect(users[0].email).toEqual(testEmail);
  });

  it('finds a user by id', async () => {
    const user = await controller.findUser('1');
    expect(user).toBeDefined();
  });

  it('finduser throws an error if user not found', async () => {
    fakeUsersService.findOne = () => null;
    await expect(controller.findUser('1')).rejects.toThrow(NotFoundException);
  });

  it('deletes a user by id', async () => {
    const user = await controller.removeUser('1');
    expect(user.id).toEqual(1);
  });

  it('updates a user by id', async () => {
    const user = await controller.updateUser('1', {
      email: testEmail,
      password: 'testPass',
    });
    expect(user.email).toEqual(testEmail);
    expect(user.password).toEqual('testPass');
  });

  it('signin updates session object and returns user', async () => {
    const session = { userId: null };
    const user = await controller.signin(
      {
        email: testEmail,
        password: 'testPass',
      },
      session,
    );
    expect(user.id).toEqual(1);
    expect(session.userId).toEqual(1);
  });
});
